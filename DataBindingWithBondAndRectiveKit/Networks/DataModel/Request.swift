//
//  Request.swift
//  SampleProject
//
//  Created by Qasim Naveed on 15/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation

public class BaseRequest: Codable {
    
}

public class LoginRequest: Codable {
    public var email: String?
    public var password: String?
    public var app_version: String?
    public var device_id: String?
    public var device_name: String?
    public var device_model: String?
    public var os_name: String?
    public var os_version: String?
    public var pn_type: String?
    public var pn_token: String?
    
}

public class SingleRequest: Codable {
    public var oid: String?
}

public class ListRequest: Codable {
    
    public var columns = [String]()
//    public var multiWhere: WhereData?
    public var limit: Int?
    public var offset: Int?
//    public var order: [OrderData]?
    public var search: String?
    
    
//    public class WhereData: Codable {
//        public var  column: String?
//        public var  search: [String]?
//        public var op: String?
//        public var order: [OrderData]?
//        public var children: [WhereData]?
    
//        init() {
//
//        }
//
//        init(column:String, value:String) {
//            self.column = column
//            self.op = "eq"
//            self.search = value
//        }
//
//        init(column:String, value:String, op:String ) {
//            self.column = column
//            self.op = op
//            self.search = value
//        }
//
//
//        public func addTodictionary()->NSDictionary {
//            let dictionary = NSMutableDictionary()
//            dictionary.setValue(self.column, forKey: "column")
//            dictionary.setValue(self.op, forKey: "op")
//            dictionary.setValue(self.search, forKey: "search")
//
//
//            return dictionary
//        }
//    }
    
//    
//    public class OrderData: Codable {
//        var column: String?
//        var dir: String?
//    }
    
    
//    public func addTodictionary()->NSDictionary {
//        let dictionary = NSMutableDictionary()
//        dictionary.setValue(self.columns, forKey: "columns")
//        dictionary.setValue(self.limit, forKey: "limit")
//        dictionary.setValue(self.offset, forKey: "offset")
//        dictionary.setValue(self.search, forKey: "search")
//        dictionary.setValue(self.multiWhere?.addTodictionary(), forKey: "where")
//
//        return dictionary
//    }
    
    
}

public class RegisterRequest: Codable
{
    var email: String?
    var full_name: String?
    var password: String?
    var mobile: String?
    var telco_id: Int?
    var birth_date: Int?
    var gender: String?
}

public class ResetPasswordRequest: Codable
{
    var email: String?
    var code: String?
    var password: String?
}

public class ForgotPasswordRequest: Codable
{
    var email: String?
}


