//
//  LoginDataModel.swift
//  SampleProject
//
//  Created by Qasim Naveed on 20/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation

public class LoginData: Codable {
    
    var expiry: Int?
    var Token: String?
}
