//
//  DeviceModel.swift
//  SampleProject
//
//  Created by Qasim Naveed on 16/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation
import ReactiveKit
import Bond



public class Device: Codable {
    
//    var __id__: Observable<String>?
//    var __status__: Observable<String>?
//    var device_id: Observable<Int>?
//    var device_name: Observable<String>?
//    var device_model: Observable<String>?
//    var device_type_id: Observable<String>?
//    var device_type: Observable<DeviceType>?
//
//
    

    var __id__: String?
    var __status__: String?

    var device_id: Int?
    var device_name: String?
    var device_model: String?

    var device_type_id: Int?
    var device_type: DeviceType?

    var deviceType: String?

    private enum CodingKeys : String, CodingKey {
        case __id__, __status__
        case device_id, device_name, device_model, device_type
        case deviceType = "device_type.device_type"
        case device_type_id = "device_type_id"
    }

    static func listColumns() -> [String] {
        let columns = [
            "device_id",
            "device_name",
            "device_model",
            "device_type.device_type",
            "device_type_id"
        ]

        return columns;
    }
}



