//
//  DeviceTypeModel.swift
//  SampleProject
//
//  Created by Qasim Naveed on 16/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation

public class DeviceType: Codable {
    var __id__: String?
    var __status__: String?
    
    var device_type_id: Int?
    var device_type: String?
}
