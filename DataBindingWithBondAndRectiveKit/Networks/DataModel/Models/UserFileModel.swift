//
//  UserModel.swift
//  SampleProject
//
//  Created by Qasim Naveed on 21/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation


public class UserFile: Codable {
    
    var file_id: Int?
    var file_name: String?
    var file_size: Int?
    var file_type: String?
    var file_url: String?
    var is_public: Int?
    var date_added: Int?
    var date_updated: Int?
    var user_id: Int?
   
    static func listColumns() -> [String] {
        let columns = [
            "file_name",
            "file_size",
            "file_type",
            "file_url"
        ]
        
        return columns;
    }
    
}


