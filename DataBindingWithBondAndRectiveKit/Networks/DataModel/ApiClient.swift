//
//  ApiClient.swift
//  SampleProject
//
//  Created by Qasim Naveed on 15/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation

public class ApiClient {

    let baseUrl = "http://13.114.16.198:9909"
    var authToken: String?

    init() {
        self.authToken = "ZavbtU5jf0rolVqLoiyGIvue1UYEgdBYpYcLcYe2Yz5ckjN6+FmGI2FuNrUt0LZX9fcn5yvHfigjsFWb2Un0ZbopeDTL771KwUvDzgtTsqLgqrEAcc7wSeHJv4TkZq3XFC8Fx/O9FeWPSGmpscF8wm3tRVS2uZ3tCoCGPDsr+Z8="
    }
    
    func unAuthorizedRequest(url: String, data: Data?, onError: @escaping (ErrorResponse?) -> Swift.Void, onSuccess: @escaping (Data?) -> Swift.Void) {
        
        var urlRequest = URLRequest(url: URL(string: ("\(self.baseUrl)\(url)"))!)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = data
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            guard error == nil else
            {
                print("Error \(error?.localizedDescription ?? "1")")
                let error = ErrorResponse(status: "Error", code: -1, message: error?.localizedDescription)
                onError(error)
                return
            }
            guard response != nil else
            {
                print(">>>not returning respnse")
                let error = ErrorResponse(status: "Error", code: -1, message: error?.localizedDescription)
                onError(error)
                return
            }
            onSuccess(data)
        }
        task.resume()
    }
    
    
    func authorizedRequest(url: String, data: Data?, onError: @escaping (ErrorResponse?) -> Swift.Void, onSuccess: @escaping (Data?) -> Swift.Void) {
        
        var urlRequest = URLRequest(url: URL(string: ("\(self.baseUrl)\(url)"))!)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(self.authToken, forHTTPHeaderField: "Authorization")
        urlRequest.httpBody = data
        
        let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in
            DispatchQueue.main.async {
                guard error == nil else
                {
                    print("Error \(error?.localizedDescription ?? "1")")
                    let error = ErrorResponse(status: "Error", code: -1, message: error?.localizedDescription)
                    onError(error)
                    return
                }
                guard response != nil else
                {
                    print("not returning respnse")
                    let error = ErrorResponse(status: "Error", code: -1, message: error?.localizedDescription)
                    onError(error)
                    return
                }
                
                onSuccess(data)
            }
        })
        task.resume()
   }
    
    
    func imageAuthorizedRequest(url: String, boundary: String, data: Data?, onError: @escaping (ErrorResponse?) -> Swift.Void, onSuccess: @escaping (Data?) -> Swift.Void) {
        
        var urlRequest = URLRequest(url: URL(string: ("\(self.baseUrl)\(url)"))!)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(self.authToken, forHTTPHeaderField: "Authorization")
        urlRequest.httpBody = data
        
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                guard error == nil else
                {
                    print("Error \(error?.localizedDescription ?? "1")")
                    let error = ErrorResponse(status: "Error", code: -1, message: error?.localizedDescription)
                    onError(error)
                    return
                }
                guard response != nil else
                {
                    print("not returning respnse")
                    let error = ErrorResponse(status: "Error", code: -1, message: error?.localizedDescription)
                    onError(error)
                    return
                }
                
                onSuccess(data)
            }
            
            task.resume()
        
    }
    
}
