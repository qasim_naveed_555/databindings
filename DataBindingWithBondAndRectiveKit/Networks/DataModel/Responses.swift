//
//  Responses.swift
//  SampleProject
//
//  Created by Qasim Naveed on 16/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation

public class GenericResponse: Codable {
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
    
    init(status: String, code: Int?, message: String?) {
        self.Status = status
        self.ErrorCode = code
        self.ErrorMessage = message
    }
}
public class loginResponse: Codable {
    var data: LoginData?
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
   
}
public class ErrorResponse: GenericResponse {
    
}

public class DeviceListResponse: Codable {
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
    
    var data: [Device]?
    var total_records: Int?
    
    
}
public class UserFileResponse: Codable {
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
    
    var data: [UserFile]?
    var total_records: Int?
    
}
public class UploadImageResponse: Codable {
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
    
    var data: UserFile?
    var total_records: Int?
    
}

/*{"Status": "Ok", "data": {"file_type": "image", "date_updated": 1511443894, "file_name": "UserFile", "file_size": 18752501, "file_url": "http://13.114.16.198:9909/files/5/MjA", "file_id": 20, "date_added": 1511443894, "is_public": 0, "user_id": 5}}*/



public class DeviceSingleResponse: Codable {
    var data: Device?
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
    
}



public class RegistrationResponse: Codable {
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
}

public class ForgotPasswordResponse: Codable {
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
}

public class ResetPasswordResponse: Codable {
    var Status: String?
    var ErrorCode: Int?
    var ErrorMessage: String?
    var statusMessage: String?
    
}
  /*  private enum CodingKeys: CodingKey {
        case Token
    }

    required public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.Token = try container.decode(String.self, forKey: .Token)
        try super.init(from: decoder)
    }*/

