//
//  AuthService.swift
//  SampleProject
//
//  Created by Qasim Naveed on 22/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation


public class AuthService: ApiClient {
    
    static let sharedInstance = AuthService()
    
    
    func Login(request: LoginRequest, onSuccess: @escaping (loginResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(request)
            
            self.unAuthorizedRequest(url: "/auth/Login", data: data, onError: onError, onSuccess: { (data) in
                
                let decoder = JSONDecoder()
                do {
                    let response: loginResponse = try decoder.decode(loginResponse.self, from: data!)
                    if response.Status == "Error" {
                        let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                        onError(error)
                        return
                    }
                    
                    onSuccess(response)
                }
                catch {
                    let error = ErrorResponse(status: "Error", code: -1, message: "ApiService=Parsing Error")
                    onError(error)
                    return
                }
            })
        }
        catch {
            let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
            onError(error)
            return
        }
        
    }
    
    func Register(request: RegisterRequest, onSuccess: @escaping (RegistrationResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(request)
            
            self.unAuthorizedRequest(url: "/auth/Register", data: data, onError: onError, onSuccess: { (data) in
                
                let decoder = JSONDecoder()
                do {
                    let response: RegistrationResponse = try decoder.decode(RegistrationResponse.self, from: data!)
                    if response.Status == "Error" {
                        let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                        onError(error)
                        return
                    }
                    
                    onSuccess(response)
                }
                catch {
                    let error = ErrorResponse(status: "Error", code: -1, message: "ApiService=Parsing Error")
                    onError(error)
                    return
                }
            })
        }
        catch {
            let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
            onError(error)
            return
        }
        
    }
    
    func ResetPassword(request: ResetPasswordRequest, onSuccess: @escaping (ResetPasswordResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(request)
            
            self.unAuthorizedRequest(url: "/auth/ResetPassword", data: data, onError: onError, onSuccess: { (data) in
                
                let decoder = JSONDecoder()
                do {
                    let response: ResetPasswordResponse = try decoder.decode(ResetPasswordResponse.self, from: data!)
                    if response.Status == "Error" {
                        let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                        onError(error)
                        return
                    }
                    
                    onSuccess(response)
                }
                catch {
                    let error = ErrorResponse(status: "Error", code: -1, message: "ApiService=Parsing Error")
                    onError(error)
                    return
                }
            })
        }
        catch {
            let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
            onError(error)
            return
        }
        
    }
    
    func ForgetPassword(request: ForgotPasswordRequest, onSuccess: @escaping (ForgotPasswordResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(request)
            
            self.unAuthorizedRequest(url: "/auth/ForgotPassword", data: data, onError: onError, onSuccess: { (data) in
                
                let decoder = JSONDecoder()
                do {
                    let response: ForgotPasswordResponse = try decoder.decode(ForgotPasswordResponse.self, from: data!)
                    if response.Status == "Error" {
                        let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                        onError(error)
                        return
                    }
                    
                    onSuccess(response)
                }
                catch {
                    let error = ErrorResponse(status: "Error", code: -1, message: "ApiService=Parsing Error")
                    onError(error)
                    return
                }
            })
        }
        catch {
            let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
            onError(error)
            return
        }
        
    }
}
