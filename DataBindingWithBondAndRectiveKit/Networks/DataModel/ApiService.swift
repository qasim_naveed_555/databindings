//
//  Apiservice.swift
//  SampleProject
//
//  Created by Qasim Naveed on 15/11/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation

public class ApiService: ApiClient {
    
    static let sharedInstance = ApiService()
    
    
    func getDeviceSingle(request: SingleRequest, onSuccess: @escaping (DeviceSingleResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {

        let encoder = JSONEncoder()

        do {
            let data = try encoder.encode(request)
            
            self.authorizedRequest(url: "/api/devices/Single", data: data, onError: onError) { (data) in
                
                let decoder = JSONDecoder()
                
                do {
                    let response: DeviceSingleResponse = try decoder.decode(DeviceSingleResponse.self, from: data!)
                    if response.Status == "Error" {
                        let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                        onError(error)
                        return
                    }
                    
                    onSuccess(response)
                }
                catch {
                    let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
                    onError(error)
                    return
                }
            }
            
        }
        catch {
            let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
            onError(error)
            return
        }
        
        
    }
    
    func getDeviceList(request: ListRequest, onSuccess: @escaping (DeviceListResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(request)
        
            self.authorizedRequest(url: "/api/devices/List", data: data, onError: onError) { (data) in
                
                let decoder = JSONDecoder()
                
                do {
                    let response = try decoder.decode(DeviceListResponse.self, from: data!)
                   
                    if response.Status == "Error" {
                        let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                        onError(error)
                        return
                    }
                    
                    onSuccess(response)
                    
                }
                catch {
                    let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
                    onError(error)
                    return
                }
            }

        }
        catch {
            let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
            onError(error)
            return
        }
        
    }
    
    

    func getImagesList(request: ListRequest, onSuccess: @escaping (UserFileResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {
        
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(request)
            
            self.authorizedRequest(url: "/api/user_files/List", data: data, onError: onError) { (data) in
                
               // let decoder = JSONDecoder()
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    print(json!)
                    let response = try JSONDecoder().decode(UserFileResponse.self, from: data!)
                    
                    if response.Status == "Error" {
                        let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                        onError(error)
                        return
                    }
                    
                    onSuccess(response)
                    
                }
                catch {
                    let error = ErrorResponse(status: "Error", code: -1, message: "<<Parsing Error")
                    onError(error)
                    return
                }
            }
            
        }
        catch {
            let error = ErrorResponse(status: "Error", code: -1, message: ">>Parsing Error")
            onError(error)
            return
        }
        
    }

    func UploadImage(request: Data, onSuccess: @escaping (UploadImageResponse?) -> Swift.Void, onError: @escaping (ErrorResponse?) -> Swift.Void) {
        
        let boundary = "Boundary-\(UUID().uuidString)"
        
        let body = NSMutableData()
        let fname = "UserFile"
        let mimetype = "image/png"
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"UserFile\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(request)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        
        self.imageAuthorizedRequest(url: "/api/user_files/Upload", boundary: boundary, data: body as Data, onError: onError) { (data) in
            
            let decoder = JSONDecoder()
            
            do {
                if let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                {
                    print(dataString)
                    
                }
                let response: UploadImageResponse = try decoder.decode(UploadImageResponse.self, from: data!)
                if response.Status == "Error" {
                    let error = ErrorResponse(status: "Error", code: response.ErrorCode, message: response.ErrorMessage)
                    onError(error)
                    return
                }
                
                onSuccess(response)
            }
            catch {
                let error = ErrorResponse(status: "Error", code: -1, message: "Parsing Error")
                onError(error)
                return
            }
        }
    }
    
}
