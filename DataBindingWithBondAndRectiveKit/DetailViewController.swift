//
//  DetailViewController.swift
//  DataBindingWithBondAndRectiveKit
//
//  Created by Qasim Naveed on 06/12/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond

class DetailViewController: UIViewController {
    
    @IBOutlet weak var deviceType: UITextField!
    @IBOutlet weak var deviceModel: UITextField!
    @IBOutlet weak var deviceName: UITextField!
    
    let deviceViewModel = singeDeviceModel()
    
    var device = Device()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bind()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func bind() {
        
        
        deviceViewModel.deviceModel.bind(to: deviceModel.reactive.text)
        deviceViewModel.deviceName.bind(to: deviceName.reactive.text)
        deviceViewModel.deviceType.bidirectionalBind(to: deviceType.reactive.text)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
