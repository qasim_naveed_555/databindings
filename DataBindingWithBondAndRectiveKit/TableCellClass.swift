//
//  TableCellClass.swift
//  DataBindingWithBondAndRectiveKit
//
//  Created by Qasim Naveed on 05/12/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import UIKit

class TableCellClass: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setDeviceImageUrl(str: String) {
        let url = URL(string: str)
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                self.deviceImage.image = UIImage(data: data!)
            }
        }
    }
}
