//
//  ViewController.swift
//  DataBindingWithBondAndRectiveKit
//
//  Created by Qasim Naveed on 04/12/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond


class ViewController: UIViewController {
    
    @IBOutlet weak var Label: UITextField!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var tableViewResult: UITableView!
    
    private let dataFromViewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }

    func bindViewModel() {
        
        
        
        dataFromViewModel.searchString.bidirectionalBind(to: Label.reactive.text)
        
        
        
        
        
        
        dataFromViewModel.validSearchText
            .map { $0 ? .black : .red }
            .bind(to: Label.reactive.textColor)
      
        dataFromViewModel.Devices.bind(to: tableViewResult) {  dataSource, indexPath, dataTable in
            let cell = dataTable.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! TableCellClass
            let device = dataSource[indexPath.row]
            
            cell.nameLabel.text = device.device_name
            cell.typeLabel.text = device.device_model
            cell.setDeviceImageUrl(str: "http://13.114.16.198:9909/files/1/MQ.jpg")
        
            return cell
            
        }
    }

    @IBAction func GetData(_ sender: UIButton) {
        dataFromViewModel.searchString.bind(to:value.reactive.text)
    }
}
