//
//  SingleDeviceModel.swift
//  DataBindingWithBondAndRectiveKit
//
//  Created by Qasim Naveed on 06/12/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation
import ReactiveKit
import Bond


class singeDeviceModel {
    var device = Device()
    
    
    
    let deviceName = Observable<String?>("")
    let deviceModel = Observable<String?>("")
    let deviceType = Observable<String?>("")
    
    init() {
        getDeviceSingle()
        
        _ = deviceType.observeNext(with: { (text) in
            print("\(text!)")
        })
    }

    func getDeviceSingle() {
        let apiService = ApiService.sharedInstance
        let singleRequest = SingleRequest()
        singleRequest.oid = "1"
        apiService.getDeviceSingle(request: singleRequest, onSuccess: { (response: DeviceSingleResponse?) in
            DispatchQueue.main.async {
                
                self.device = (response?.data)!
                
                self.deviceName.value = response?.data?.device_name
                self.deviceModel.value = response?.data?.device_model
                self.deviceType.value = response?.data?.device_type?.device_type
                
                print("String")
            }
            
        }) { (error) in
            
        }
        
    }
    
    
}

