//
//  ViewModel.swift
//  DataBindingWithBondAndRectiveKit
//
//  Created by Qasim Naveed on 04/12/2017.
//  Copyright © 2017 Qasim Naveed. All rights reserved.
//

import Foundation
import ReactiveKit
import Bond

class ViewModel {
    
    let searchString = Observable<String?>("")
    let validSearchText = Observable<Bool>(false)
    let Devices = MutableObservableArray<Device>([])
    
    init() {
        searchString
            .map { $0!.characters.count > 3 }
            .bind(to:validSearchText)
        
        _ = searchString
            .filter { $0!.characters.count > 3 }
            .throttle(seconds: 0.5)
            .observeNext {
                [unowned self] text in
                if let text = text {
                    self.getDevice(coulmns: Device.listColumns(), offset: 0, search: text)
                }
        }
        
        _ = searchString
            .filter{ $0!.characters.count > 0 && $0!.characters.count < 3}
            .throttle(seconds: 0.5)
            .observeNext(with: { _ in
                self.getDevice(coulmns: Device.listColumns(), offset: 0, search: "")
            })
        self.getDevice(coulmns: Device.listColumns(), offset: 0, search: "")
    }
    
    func getDevice(coulmns: [String], offset: Int, search: String) {
        let apiService = ApiService.sharedInstance
        let listrequest = ListRequest()
        listrequest.columns = coulmns
        listrequest.limit = nil
        listrequest.offset = offset
        listrequest.search = search
        
        apiService.getDeviceList(request: listrequest, onSuccess: { [unowned self] (response: DeviceListResponse?) in
            print("\((response)!)")
            let comingDevices = (response?.data)!
            self.Devices.removeAll()
            self.Devices.insert(contentsOf: comingDevices, at: 0)
            print(self.Devices)
            
        }) { (res) in
            
            print(res!.ErrorMessage!)
        }
        print(self.Devices)
    }
}
